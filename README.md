# Tarea 1 Sistemas Distribuidos

El presente repositorio contiene los archivos necesarios para la resolucion de la tarea 1, junto con las instrucciones de uso en el archivo readme.
El trabajo fue desarollado en javaScript

## Instrucciones para ejecutar los contenedores docker

Una vez descargado el directorio que contiene los archivos, se debe acceder al directorio /buscador.

> `cd /buscador`

Este directorio posee los archivos correspondientes para el funcionamiento del Buscador de Inventario. Entre los archivos que se encuentran estan:

- gRPC_client.js: Cliente gRPC, contiene la conexion al servidor gRPC.
- proto/struct.proto: Contiene el proto bufer utilizado para la comunicacion gRPC.
- index.js: Contiene la configuracion de la API REST (Cliente Redis, y utiliza el cliente gRPC).
- dockerfile: Contiene la receta de la imagen del Buscador de Inventario.

Para crear la imagen del Buscador, debe ejecutar el siguiente comando:

> `sudo docker build -t buscador .`

Este comando construira la imagen docker, llamada "buscador", para ser utilizada posteriormente por el docker-compose.

Luego, se debe acceder al directorio /inventario.

> `cd ..`

> `cd /inventario`

Este directorio posee los archivos correspondientes para el funcionamiento del Inventario. Entre los archivos que se encuentran estan:

- psqlConect.js: Cliente PostgreSQL, contiene la conexion a PostgreSQL 
- gRPC_server.js: Contiene la configuracion del servicio del Inventario (Servidor gRPC, y utiliza el cliente PostgreSQL)
- proto/struct.proto: Contiene el proto bufer utilizado para la comunicacion gRPC.
- dockerfile: Contiene la receta de la imagen del servicio del Inventario.

Para crear la imagen del Inventario, se debe ejecutar el siguinte comando:

> `sudo docker build -t inventario .`

Este comando construira la imagen docker, llamada "inventario", que sera utilizada posteriormente por el docker-compose.

Finalmente, y para ejecutar de forma completa el trabajo, se debe acceder al directorio principal.

> `cd ..`

Este directorio posee un docker-compose, el cual posee la receta para crear los 4 contenedores solicitados en la tarea:
- 1) PostgreSQL
- 2) Redis
- 3) Inventario
- 4) Buscador de Inventario 

Este archivo utilizas las dos imagenes indicadas en la tarea (postgres y redis), mientras que ocupa las otras dos creadas previamente por los dockerfile. 
Para crear los servicios se debe ejecutar el siguiente comando:

> `sudo docker-compose up -d`

El cual ejecutara todas las imagenes que se referencian en el archivo docker-compose.yml.

Importante!!! 

Verificar que se encuentran arriba los 4 contenedores necesarios utilizando el comando:

> `sudo docker ps`

## Instrucciones para configuracion de limite memoria redis db

Una vez iniciados todos los contenedores requeridos, se debe acceder a la consola del contenedor que contiene la imagen de redis.
Para ello, se debe ejecutar el comando: 

> `sudo docker exec -it redis bash`

Una vez dentro del contenedor redis, se debe acceder a la consola del cliente redis utilizando el siguiente comando:

> `redis-cli`

En este punto, ya nos encontramos dentro de la base de datos de Redis.

Para configurar el limite de memoria, se debe ingresar el siguiente comando dentro de la consola redis-cli:

> `CONFIG SET maxmemory {numero de megabytes desados}mb`

por ejemplo:
> `CONFIG SET maxmemory 5mb`

De esta forma, logramos limitar la memoria de la base de datos para el cache.

## Instrucciones para configuracion de politica de remocion (LRU o LFU) en redis db

Para determinar la politica de remocion en el caso de que se sobrepase la memoria maxima, se debe utilizar el siguiente comando en la consola de redis:

Para LRU

> `CONFIG SET maxmemory-policy allkeys-lru`

Para LFU

> `CONFIG SET maxmemory-policy allkeys-lfu`

## Verificacion de datos ingresados para la configuracion de Redis db

Para verificar si los datos fueron ingresados correctamente en la configuracion de redis, es necesario ejecutar los siguientes comandos:

Para verificar limite de memoria:

> `CONFIG GET maxmemory`

Para verificar politica de remocion:

> `CONFIG GET maxmemory-policy`

Al ejecutar dichos comandos, se nos imprimira por consola la configuracion aplicada dentro de redis en ese momento.

## Comparativas de funcionamiento para ambas politicas de remocion

En cuanto al funcionamiento de ambos algoritmos de remocion, se pueden interpretar de la siguiente forma:

LRU

Simplemente elimina el ultimo elemento solicitado del listado de consultas de cache, lo cual es poco eficiente a la larga, ya que no se utiliza ninguna especie de filtro para la eliminacion. Es decir, si tenemos como ultimo elemento del listado al dato más frecuentemente consultado, este seria eliminado, dado que se ubica en el ultimo lugar de las consultas recientes.

LFU

Esta politica de remocion, elimina el dato que menos se ha repedito en el tiempo (el menos frecuentemente consultado), lo cual es a la larga mucho mas conveniente, ya que las consultas unicas (que recien ingresan a cache), no afectan a aquellas que tienen una mayor frecuencia de consulta a largo plazo, pues puede que queden mas "atras" en el listado (inlusive al ultimo), sin embargo, no seran eliminadas (lo que es bueno, ya que el dato es frecuentemente consultado).

En base a las interpretaciones anteriores, es que consideramos como mejor opcion a implementar, la politica de LFU, dada su eficiencia y logica de funcionamiento descrita anteriormente.

Documentacion oficial de redis db para la informacion presentada
https://redis.io/docs/manual/eviction/

## Uso del Buscador

El servicio del Buscador de Invetario, se encuentra en el puerto 3000 del localhost, donde para acceder a la metodo search, se debe solicitar la ruta /search:

> http://localhost:3000/search/

y para buscar una palabra, se solicita la palabra a buscar como ruta (/"palabra a buscar").
Por ejemplo, si queremos buscar "Mens":

> http://localhost:3000/search/Mens

Lo cual retornará todos los datos asociados por esa palabra.

Las respuestas que provengan de la Base de Datos (postgresql), vendran dadas por la llave "Search_Result_BD", mientras que las respuestas que provengan del Cache (redis), vendran dadas por la llave "Search_Result_Cache".
